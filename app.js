const express = require('express');
const cors = require('cors');
const cron = require('node-cron');
const indexRouter = require('./api/routes/index');
const checkTimeExpiry = require('./service/checkTimeExpiry');

const app = express();

app.use(express.json());
app.use(cors());

app.use('/api', indexRouter);

app.use((req, res) => {
  res.status(400).json({
    status: 'error',
    message: 'URL not found',
  });
});

cron.schedule('* * * * * *', () => {
  checkTimeExpiry.checkTimeExpiry();
});

module.exports = app;
