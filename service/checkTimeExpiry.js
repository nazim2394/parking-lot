const moment = require('moment');
const models = require('../api/models');

exports.checkTimeExpiry = async function () {
  const parkingAllotment = await models.ParkingAllotment.findAll({
    where: {
      isExpired: false,
    },
  });
  for (const ele of parkingAllotment) {
    const booked_time = new Date(ele.booked_time);
    const expiry_time = moment(
      `${booked_time.getDate()}.${
        booked_time.getMonth() + 1
      }.${booked_time.getFullYear()} ${booked_time.getHours()}.${booked_time.getMinutes()}.${booked_time.getSeconds()}`,
      'DD.MM.YYYY HH.mm.ss'
    ).add(ele.wait_time, 'm');
    const now = new Date();
    var currentDate = moment(
      `${now.getDate()}.${
        now.getMonth() + 1
      }.${now.getFullYear()} ${now.getHours()}.${now.getMinutes()}.${now.getSeconds()}`,
      'DD.MM.YYYY HH.mm.ss'
    );
    var diff = expiry_time.diff(currentDate);
    const timeDiff = moment.duration(diff).minutes();
    console.log(timeDiff);
    if (timeDiff <= 0) {
      const parkingLot = await models.ParkingLot.findAll();
      const updatedParkingData = {
        no_of_cars_reserved_parking: 0,
        reserved_parking_capacity: parkingLot[0].reserved_parking_capacity,
        no_of_cars_not_reserved_parking: 0,
        not_reserved_parking_capacity:
          parkingLot[0].not_reserved_parking_capacity,
      };
      if (ele.parking_type === 'reserved') {
        updatedParkingData.no_of_cars_reserved_parking =
          parkingLot[0].no_of_cars_reserved_parking - 1;
        updatedParkingData.reserved_parking_capacity =
          parkingLot[0].reserved_parking_capacity + 1;
      } else if (ele.parking_type === 'general') {
        updatedParkingData.no_of_cars_not_reserved_parking =
          parkingLot[0].no_of_cars_not_reserved_parking - 1;
        updatedParkingData.not_reserved_parking_capacity =
          parkingLot[0].not_reserved_parking_capacity + 1;
      }
      await models.ParkingLot.update(updatedParkingData, {
        where: {
          id: parkingLot[0].id,
        },
      });
      await models.ParkingAllotment.update(
        { isExpired: true },
        { where: { id: ele.id } }
      );
    }
  }
};
