'use strict';

module.exports = (sequelize, DataTypes) => {
  const ParkingLot = sequelize.define('ParkingLot', {
    total_parking_slot: DataTypes.INTEGER,
    reserved_parking_capacity: DataTypes.INTEGER,
    not_reserved_parking_capacity: DataTypes.INTEGER,
    no_of_cars_reserved_parking: DataTypes.INTEGER,
    no_of_cars_not_reserved_parking: DataTypes.INTEGER
  });

  return ParkingLot;
};