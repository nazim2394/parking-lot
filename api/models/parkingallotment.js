'use strict';

module.exports = (sequelize, DataTypes) => {
  const ParkingAllotment = sequelize.define('ParkingAllotment', {
    is_reserved: DataTypes.BOOLEAN,
    booked_time: DataTypes.STRING,
    arrived_at: DataTypes.STRING,
    parking_number: DataTypes.INTEGER,
    wait_time: DataTypes.STRING,
    parking_type: DataTypes.STRING,
    isExpired: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    user_id: DataTypes.INTEGER,
  });

  ParkingAllotment.associate = function (models) {
    ParkingAllotment.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user',
    });
  };

  return ParkingAllotment;
};
