const moment = require('moment');
const models = require('../models');

exports.getAllInformation = async (req, res) => {
  try {
    const parkingLot = await models.ParkingLot.findAll();
    if (parkingLot) {
      res.send({
        status: 'success',
        data: {
          parkingLot,
        },
      });
    } else {
      res.send({
        status: 200,
        message: 'No Parking Lot information found',
      });
    }
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};

exports.createParkingLot = async (req, res) => {
  try {
    const data = {
      reserved_parking_capacity: (req.body.total_parking_slot / 100) * 20,
      not_reserved_parking_capacity: (req.body.total_parking_slot / 100) * 80,
      no_of_cars_reserved_parking: 0,
      no_of_cars_not_reserved_parking: 0,
      total_parking_slot: req.body.total_parking_slot,
    };

    const parkingLot = await models.ParkingLot.create(data);
    res.status(201).json({
      status: 'success',
      data: {
        parkingLot,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};

exports.updateParkingLot = async (req, res) => {};

exports.parkingAllotment = async (req, res) => {
  try {
    const parkingLot = await models.ParkingLot.findAll();
    const parking_number =
      req.body.user_id + Number(Math.floor(Math.random() * 1000 + 1));
    const updatedParkingData = {
      no_of_cars_reserved_parking: 0,
      reserved_parking_capacity: parkingLot[0].reserved_parking_capacity,
      no_of_cars_not_reserved_parking: 0,
      not_reserved_parking_capacity:
        parkingLot[0].not_reserved_parking_capacity,
    };
    const data = {
      is_reserved: req.body.is_reserved,
      booked_time: req.body.booked_time,
      arrived_at: req.body.arrived_at,
      parking_number: parking_number,
      parking_type: req.body.allotment,
      user_id: req.body.user_id,
      wait_time: req.body.waitTime,
    };
    if (req.body.allotment === 'reserved') {
      updatedParkingData.no_of_cars_reserved_parking =
        parkingLot[0].no_of_cars_reserved_parking + 1;
      updatedParkingData.reserved_parking_capacity =
        parkingLot[0].reserved_parking_capacity - 1;
    } else if (req.body.allotment === 'general') {
      updatedParkingData.no_of_cars_not_reserved_parking =
        parkingLot[0].no_of_cars_not_reserved_parking + 1;
      updatedParkingData.not_reserved_parking_capacity =
        parkingLot[0].not_reserved_parking_capacity - 1;
    }
    const parkingData = await models.ParkingAllotment.create(data);
    await models.ParkingLot.update(updatedParkingData, {
      where: {
        id: parkingLot[0].id,
      },
    });
    res.status(201).json({
      status: 'success',
      data: {
        parkingData,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};

exports.getAllAvalaibleParking = async (req, res) => {
  try {
    const availableParking = await models.ParkingLot.findAll();
    const data = {
      reserved_parking_available: availableParking[0].reserved_parking_capacity,
      non_reserved_parking_available:
        availableParking[0].not_reserved_parking_capacity,
      total_parking_available:
        availableParking[0].reserved_parking_capacity +
        availableParking[0].not_reserved_parking_capacity,
    };
    res.status(200).json({
      status: 'success',
      data: {
        availableParking: data,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};

exports.getAllOccupiedParking = async (req, res) => {
  try {
    const occupiedParking = await models.ParkingLot.findAll();
    const data = {
      reserved_parking_occupied: occupiedParking[0].no_of_cars_reserved_parking,
      non_reserved_parking_occupied:
        occupiedParking[0].no_of_cars_not_reserved_parking,
      total_parking_occupied:
        occupiedParking[0].no_of_cars_reserved_parking +
        occupiedParking[0].no_of_cars_not_reserved_parking,
    };
    res.status(200).json({
      status: 'success',
      data: {
        occupiedParking: data,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};
