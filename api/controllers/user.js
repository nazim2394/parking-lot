const models = require('../models');

exports.createUser = async (req, res) => {
  try {
    const data = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
    };
    const user = await models.User.create(data);
    res.status(201).json({
      status: 'success',
      data: {
        user,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};

exports.getAllRegisteredUsers = async (req, res) => {
  try {
    const userCount = await models.User.count();
    const userWithParkingRegistered = await models.ParkingAllotment.count({
      where: { isExpired: false },
      distinct: 'user_id',
    });
    res.status(200).json({
      status: 'success',
      data: {
        total_registered_users: userCount,
        total_users_with_parking_registered: userWithParkingRegistered,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: 'error',
      message: 'Bad Request',
    });
  }
};
