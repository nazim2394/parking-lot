const express = require('express');
const parkingLot = require('./parkingLot');
const user = require('./user');

const router = express.Router();

router.use('/parking', parkingLot);
router.use('/users', user)

module.exports = router;