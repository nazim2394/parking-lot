const express = require('express');
const user = require('../controllers/user');

const router = express.Router();

router
  .route('/')
  .get(user.getAllRegisteredUsers)
  .post(user.createUser);

module.exports = router;
