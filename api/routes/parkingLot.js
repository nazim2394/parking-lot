const express = require('express');
const parkingLot = require('../controllers/parkingLot');
const parkingValidation = require('../../middleware/parkingValidationApi');
const parkingAllotmentValidation = require('../../middleware/parkingAllotmentValidation');

const router = express.Router();

router
  .route('/')
  .get(parkingLot.getAllInformation)
  .post(parkingValidation.parkingValidationApi, parkingLot.createParkingLot)
  .put(parkingLot.updateParkingLot);

router
  .route('/add')
  .post(
    [
      parkingAllotmentValidation.bookingTimeCheck,
      parkingAllotmentValidation.checkReservedSpace,
      parkingAllotmentValidation.checkAllotmentCapacity,
    ],
    parkingLot.parkingAllotment
  );

router.route('/slot/available').get(parkingLot.getAllAvalaibleParking);

router.route('/slot/occupied').get(parkingLot.getAllOccupiedParking);

module.exports = router;
