'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ParkingAllotments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      is_reserved: {
        type: Sequelize.BOOLEAN,
      },
      booked_time: {
        type: Sequelize.STRING,
      },
      arrived_at: {
        type: Sequelize.STRING,
      },
      parking_number: {
        type: Sequelize.INTEGER,
      },
      wait_time: {
        type: DataTypes.STRING,
      },
      parking_type: {
        type: DataTypes.STRING,
      },
      isExpired: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ParkingAllotments');
  },
};
