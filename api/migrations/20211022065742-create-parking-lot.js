'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ParkingLots', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      total_parking_slot: {
        type: Sequelize.INTEGER
      },
      reserved_parking_capacity: {
        type: Sequelize.INTEGER
      },
      not_reserved_parking_capacity: {
        type: Sequelize.INTEGER
      },
      no_of_cars_reserved_parking: {
        type: Sequelize.INTEGER
      },
      no_of_cars_not_reserved_parking: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ParkingLots');
  }
};