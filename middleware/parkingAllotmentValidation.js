const moment = require('moment');
const models = require('../api/models');

exports.bookingTimeCheck = (req, res, next) => {
  if (req.body.booked_time) {
    const bookDate = new Date(req.body.booked_time);
    const now = new Date();
    var bookingDate = moment(
      `${bookDate.getDate()}.${
        bookDate.getMonth() + 1
      }.${bookDate.getFullYear()} ${bookDate.getHours()}.${bookDate.getMinutes()}.${bookDate.getSeconds()}`,
      'DD.MM.YYYY HH.mm.ss'
    );
    var currentDate = moment(
      `${now.getDate()}.${
        now.getMonth() + 1
      }.${now.getFullYear()} ${now.getHours()}.${now.getMinutes()}.${now.getSeconds()}`,
      'DD.MM.YYYY HH.mm.ss'
    );
    var diff = bookingDate.diff(currentDate);
    const priorMinutes = moment.duration(diff).minutes();
    if (priorMinutes >= 15 && priorMinutes <= 16) {
      next();
    } else {
      return res.status(400).json({
        status: 'error',
        message: 'You can book a parking space 15 mins prior to arrival',
      });
    }
  } else {
    return res.status(400).json({
      status: 'error',
      message: 'Booking Time is required',
    });
  }
};

exports.checkReservedSpace = async (req, res, next) => {
  const parkingLot = await models.ParkingLot.findAll();
  if (parkingLot.length > 0) {
    if (req.body.is_reserved) {
      if (
        parkingLot[0].no_of_cars_reserved_parking ===
        parkingLot[0].reserved_parking_capacity
      ) {
        if (
          parkingLot[0].no_of_cars_not_reserved_parking ===
          parkingLot[0].not_reserved_parking_capacity
        ) {
          return res.status(404).json({
            status: 'error',
            message: 'Sorry, Queue is full',
          });
        } else {
          req.body.allotment = 'general';
          next();
        }
      } else {
        req.body.allotment = 'reserved';
        next();
      }
    } else {
      if (
        parkingLot[0].no_of_cars_not_reserved_parking ===
        parkingLot[0].not_reserved_parking_capacity
      ) {
        return res.status(404).json({
          status: 'error',
          message: 'Sorry, Queue is full',
        });
      } else {
        req.body.allotment = 'general';
        next();
      }
    }
  } else {
    return res.status(404).json({
      status: 'error',
      message: 'No Parking Details Found',
    });
  }
};

exports.checkAllotmentCapacity = async (req, res, next) => {
  const parkingLot = await models.ParkingLot.findAll();
  const parkingCapacityPercent =
    ((parkingLot.no_of_cars_reserved_parking +
      parkingLot.no_of_cars_not_reserved_parking) /
      parkingLot.total_parking_slot) *
    100;
  if (parkingCapacityPercent >= 50) {
    req.body.waitTime = 0;
    next();
  } else if (parkingCapacityPercent === 100) {
    return res.status(400).json({
      status: 'error',
      message: 'Sorry, Queue is full',
    });
  } else {
    req.body.waitTime = 15;
    next();
  }
};

// exports.allotmentStrategy = async (req, res, next) => {
//   const parkingLot = await models.ParkingLot.findAll();
// }
