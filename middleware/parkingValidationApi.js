exports.parkingValidationApi = async (req, res, next) => {
  var error = [];
  if (!req.body.total_parking_slot || req.body.total_parking_slot === '') {
    error.push('Total parking slot required');
  } else if (typeof req.body.total_parking_slot !== 'number') {
    error.push('Total parking slot must be a number');
  }
  if (error.length > 0) {
    res.status(400).json({
      status: 'error',
      message: error.join(','),
    });
  } else {
    next();
  }
};
